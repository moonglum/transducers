// Some example that can be expressed in SQL? :thinking:

let items = [
	{ name: "Potato", price: 14, amount: 20, cartId: 5 },
	{ name: "Tomato", price: 15, amount: 30, cartId: 5 },
	{ name: "Foo", price: 10, amount: 1, cartId: 7 },
	{ name: "Bar", price: 20, amount: 2, cartId: 5 },
	{ name: "Baz", price: 30, amount: 3, cartId: 3 }
]

console.dir(
	items
		.filter(item => item.cartId === 5)
		.map(item => item.price * item.amount)
		.reduce((acc, item) => acc + item)
)

let acc = 0
for (let item of items) {
	if (item.cartId === 5) {
		acc += item.price * item.amount
	}
}
console.dir(acc)
