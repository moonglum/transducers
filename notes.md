# Notes on the Hickey Talk

History in Clojure:
* https://www.clojure.org/news/2012/05/08/reducers
* https://www.clojure.org/news/2012/05/15/anatomy-of-reducer
* https://www.clojure.org/news/2014/08/06/transducers-are-coming
* https://www.youtube.com/watch?v=6mTbuzafcII

Compare with:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols

transducers:
extract the essence of map, filter et al away from the functions that transform sequences/collections, so they can be used elsewhere recasting them as *process transformations*

A reducing function is just the kind of function you’d pass to reduce - it takes
a result so far and a new input and returns the next result-so-far and a
transducer is a function that takes one reducing function and returns another.

What kinds of processes: ones that can be defined in terms of succession of steps where each step ingests an input (building a collection is just one instance).
"seeded left reduce"

reduce: lead back
ingest: carry into
transduce: lead across
(on the way back/in, will carry inputs across a series of transformations)

Problems without reducers:
* No reuse: Every new collection/process defines its own versions of map, filter et al
* Composed algorithms are needlessly specific to every collection/process
* Composed algorithms are inefficient

Functions that take a reducer:
into: build a concrete collection
sequence: build a lazy sequence
transduce: like reduce, but takes transducers
chan: a CSP channel that processes bags
observable?

Deriving transducers:
http://www.cs.ox.ac.uk/files/3390/PRG69.pdf
http://www.cs.nott.ac.uk/~pszgmh/fold.pdf

Many list functions can be defined in terms of foldr. foldr encapsulates the recursion, easier to reason about and transform.
Similarly, you can define them in terms of reduce (foldl) returnins eager, appendy vectors.
right reduce puts you on the lazyness path
left reduce puts you on the loop path
it turns out the loop path is better and faster and more general for the kinds of things we want to do especially if we can get lazyness later

Transducers are fully decoupled:
* They know nothing of the process they modify as the rducing function fully encapsulates that
* May call step 0, 1 or more times
* Must pass previous result as next r, otherwise must know nothing of r
* can transform input arg

Transducers are fast:
* Just a stack of function calls (short, inlinable)
* No laziness overhead
* No interim collections
* No extra boxes

Early Termination
* Reduction normally processes all input
* Sometimes a process has just 'had enough' inout, or gotten external trigger to terminate
* A transducer might decide the same
=> Clojure's reduce supports early termination via (reduced result). This is a wrapper around the value. You can ask if it is in the wrapper and stop. It's not a Maybe, because we only wrap in the case we are reduced.
The transducing process must support reduced. If it gets a reduced value, it stops what it is doing and returns the unwrapped value.

Some transducers require state (take, partition). They must create unique state every time they are called upon to transform a step function.

Some process complete, and will receive no more input. A process might want to do a final transformation of the value built up (example: partition). A stateful transducer might want to flush a pending value. All step functions must have a variant that only take an accumilation, but not a value). A completinng process must call the completion operation on the final accumulated value, exactly once. A transducer's completion operation mut call its nested completion operation, exactly once, and return what it returns.

A reducing function may support a variant that takes no arguments which returns an initial accumulation value.
