// console.dir(
// 	reduce((x, y) => x + y, 0, [1, 2, 3])
// );

// console.dir(
// 	reduce(
// 		map(x => x + 1, concat), [], [1, 2, 3]
// 	)
// );

// console.dir(
// 	reduce(
// 		filter(x => x % 2 === 0, concat), [], [1, 2, 3]
// 	)
// );

// console.dir(
// 	reduce(
// 		map(x => x + 1,
// 			filter(x => x % 2 === 0,
// 				(x, y) => x + y)),
// 		0,
// 		[1, 2, 3]
// 	)
// )

// console.dir(
// 	reduce(
// 		map(x => x + 1)(
// 			filter(x => x % 2 === 0)(
// 				(x, y) => x + y)),
// 		0,
// 		[1, 2, 3]
// 	)
// )

// console.dir(
// 	reduce(
// 		compose(
// 			map(x => x + 1),
// 			filter(x => x % 2 === 0),
// 		)((x, y) => x + y),
// 		0,
// 		[1, 2, 3]
// 	)
// )

console.dir(
	transduce(
		compose(
			map(x => x + 1),
			filter(x => x % 2 === 0)
		),
		(x, y) => x + y,
		0,
		[1, 2, 3]
	)
)

function transduce(transform, reducer, accumulation, enumerable) {
	let fn = transform(reducer)
	for (let current of enumerable) {
		accumulation = fn(accumulation, current)
	}
	return accumulation
}

function reduce(fn, accumulation, enumerable) {
	for (let current of enumerable) {
		accumulation = fn(accumulation, current)
	}
	return accumulation
}

function map(fn) {
	// Step 1:
	// let accumulation = [];
	// for (let current of enumerable) {
	// 	accumulation.push(fn(current))
	// }
	// return accumulation;

	// Step 2:
	// return reducer((acc, current) => {
	// 	return concat(acc, fn(current));
	// }, enumerable, []);

	// Step 3:
	// return (acc, current) => {
	// 	return concat(acc, fn(current));
	// };

	// Step 4:
	// return (acc, current) => {
	// 	return reducer(acc, fn(current));
	// };

	return reducer => (acc, current) => {
		return reducer(acc, fn(current))
	}
}

function filter(fn) {
	// Step 1:
	// let accumulation = [];
	// for (let current of enumerable) {
	// 	if(fn(current)) {
	// 		accumulation.push(current);
	// 	}
	// }
	// return accumulation;

	// Step 2:
	// return reducer((acc, current) => {
	// 	return fn(current) ? concat(acc, current) : acc;
	// }, enumerable, []);

	// Step 3:
	// return (acc, current) => {
	// 	return fn(current) ? concat(acc, current) : acc;
	// };

	// Step 4:
	// return (acc, current) => {
	// 	return fn(current) ? reducer(acc, current) : acc;
	// };

	return reducer => (acc, current) => {
		return fn(current) ? reducer(acc, current) : acc
	}
}

// concat is a reducer, but reveal that a little later
function concat(accumulation, current) {
	return accumulation.concat(current)
}

// compose is called comp in Clojure
// it's called the B-combinator or Bluebird
function compose(...functions) {
	return reduce(
		(accumulation, fn) => (...args) => accumulation(fn(...args)),
		x => x,
		functions
	)
}

// other methods:
// every
// find
// flatMap
// includes
// some
// sort ???
