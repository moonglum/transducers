// Lib:
// https://www.npmjs.com/package/trainspotting

class Reduced {
	constructor(val) {
		this.val = val
	}
}

function transduce(transform, reducer, accumulation, enumerable) {
	let fn = transform({ step: reducer })
	return reduce(fn.step, accumulation, enumerable)
}

function reduce(fn, accumulation, enumerable) {
	for (let current of enumerable) {
		accumulation = fn(accumulation, current)

		if (accumulation instanceof Reduced) {
			return accumulation.val
		}
	}
	return accumulation
}

// TODO: Does this also need to be a class "ArrayReducer"?
// This is a reducer, not a transducer
function concat(accumulation, current) {
	// "correct" would be:
	// return accumulation.concat(current)
	// but this creates new arrays, and JS doesn't have structural sharing
	accumulation.push(current)
	return accumulation
}

// compose is called comp in Clojure
// it's also called the B-combinator or Bluebird
function compose(...functions) {
	return functions.reduce((accumulation, fn) => (...args) =>
		accumulation(fn(...args))
	)
}

class MappingTransducer {
	constructor(fn, reducer) {
		this.fn = fn
		this.reducer = reducer
		this.step = this.step.bind(this)
	}

	step(accumulator, step) {
		return this.reducer.step(accumulator, this.fn(step))
	}
}

function map(fn) {
	return reducer => new MappingTransducer(fn, reducer)
}

class FilteringTransducer {
	constructor(fn, reducer) {
		this.fn = fn
		this.reducer = reducer
		this.step = this.step.bind(this)
	}

	step(accumulator, step) {
		if (this.fn(step)) {
			return this.reducer.step(accumulator, step)
		}
		return accumulator
	}
}

function filter(fn) {
	return reducer => new FilteringTransducer(fn, reducer)
}

class TakingTransducer {
	constructor(n, reducer) {
		this.i = 0
		this.n = n
		this.reducer = reducer
		this.step = this.step.bind(this)
	}

	step(accumulator, step) {
		this.i++
		if (this.i > this.n) {
			return new Reduced(accumulator)
		}
		return this.reducer.step(accumulator, step)
	}
}

function take(n) {
	return reducer => new TakingTransducer(n, reducer)
}

class DroppingTransducer {
	constructor(n, reducer) {
		this.i = 0
		this.n = n
		this.reducer = reducer
		this.step = this.step.bind(this)
	}

	step(accumulator, step) {
		this.i++
		if (this.i <= this.n) {
			return accumulator
		}
		return this.reducer.step(accumulator, step)
	}
}

function drop(n) {
	return reducer => new DroppingTransducer(n, reducer)
}

// TODO:
// every
// find
// flatMap
// includes
// some
// sort ???
// https://github.com/jlongster/transducers.js/blob/master/transducers.js#L508-L536

console.dir(
	transduce(
		compose(
			map(x => x + 1),
			filter(x => x % 2 === 0),
			drop(1),
			take(2)
		),
		// (x, y) => x + y,
		// 0,
		concat,
		[],
		[1, 2, 3, 4, 5, 6, 7]
	)
)
