// A *transformer* is a function that takes a value and returns a transformed version of it.
let toUpper = str => str.toUpperCase()
// let shout = str => `${str}!!!`
// let scream = str => toUpper(shout(str))
let double = number => number * 2
// console.log(double(4))

// can be used on arrays via the map method
// console.log([1, 2, 3, 4].map(double))

// implement map
// be aware that this map works with *any* iterable
let map = (transform, iterable) => {
	let accumulation = []
	for (let value of iterable) {
		accumulation.push(transform(value))
	}
	return accumulation
}
// console.log(map(double, [1, 2, 3, 4]))

// A *predicate* is a function that takes a value and returns a boolean
let isEven = number => number % 2 === 0
let isVowel = char => ["a", "e", "i", "o", "u"].includes(char.toLowerCase())
// console.log(isEven(10))

// can be used on arrays via the filter method
// console.log([1, 2, 3, 4].filter(isEven))

// implement filter
let filter = (predicate, iterable) => {
	let accumulation = []
	for (let value of iterable) {
		if (predicate(value)) {
			accumulation.push(value)
		}
	}
	return accumulation
}
// console.log(filter(double, [1, 2, 3, 4]))

// A *reducer* is a function that takes an accumulation and a value, and returns a new accumulation
// The type of the old and new accumulation must match
// let add = (accumulation, value) => accumulation + value
// console.log(add(add(2, 4), 6))

// can be used on arrays via the reduce method
// console.log([1, 2, 3, 4].reduce(add, 0))

// implement reduce
let reduce = (reducer, accumulation, iterable) => {
	for (let value of iterable) {
		accumulation = reducer(accumulation, value)
	}
	return accumulation
}
// console.log(reduce(add, 0, [1, 2, 3, 4]))

// We can derive map and filter from reduce

// derive map from reduce
map = (transform, iterable) => {
	return reduce(
		(accumulation, value) => {
			return accumulation.concat(transform(value))
		},
		[],
		iterable
	)
}
// console.log(map(double, [1, 2, 3, 4]))

// derive filter from reduce
filter = (predicate, iterable) => {
	return reduce(
		(accumulation, value) => {
			if (predicate(value)) {
				return accumulation.concat(value)
			}
			return accumulation
		},
		[],
		iterable
	)
}
// console.log(filter(isEven, [1, 2, 3, 4]))

// Now we can chain those calls:
// console.log(map(double, filter(isEven, [1, 2, 3, 4])))

// make these functions into transducers. This will let us compose the reducing behavior of map() and filter(), without coupling this composed transform to a data type.
// first, let's remove the call to reduce and just return the function that is passed to reduce
// In other words: map and filter return a reducer

map = transform => {
	return (accumulation, value) => {
		return accumulation.concat(transform(value))
	}
}

filter = predicate => {
	return (accumulation, value) => {
		if (predicate(value)) {
			return accumulation.concat(value)
		}
		return accumulation
	}
}
// console.log(reduce(map(double), [], reduce(filter(isEven), [], [1, 2, 3, 4])))

// this is still two iterations, what if filter took a reducer?
// this is our first transducer
filter = predicate => reducer => {
	return (accumulation, value) => {
		if (predicate(value)) {
			return reducer(accumulation, value)
		}
		return accumulation
	}
}
// console.log(reduce(filter(isEven)(map(double)), [], [1, 2, 3, 4]))

let isEvenFilter = filter(isEven)
let isNot2Filter = filter(n => n !== 2)
let doubleMap = map(double)
// console.log(reduce(isNot2Filter(isEvenFilter(doubleMap)), [], [1, 2, 3, 4]))

// let's make map a transducer as well
map = transform => reducer => {
	return (accumulation, value) => {
		return reducer(accumulation, transform(value))
	}
}

// now we need to pass something to our inner map reducer
let pushReducer = (accumulation, value) => {
	return accumulation.concat(value)
}

doubleMap = map(double)
// console.log(
// 	reduce(isNot2Filter(isEvenFilter(doubleMap(pushReducer))), [], [1, 2, 3, 4])
// )

// compose
// compose(isNot2Filter, isEvenFilter, doubleMap)(pushReducer) ===
//   isNot2Filter(isEvenFilter(doubleMap(pushReducer)))
// compose is a combinator
// it's called the B-combinator or Bluebird
let compose = (...functions) => {
	return reduce(
		(accumulation, fn) => {
			return (...args) => accumulation(fn(...args))
		},
		x => x,
		functions
	)
}

let cleanNumbersTransform = compose(isNot2Filter, isEvenFilter, doubleMap)
// console.log(reduce(cleanNumbersTransform(pushReducer), [], [1, 2, 3, 4]))

// Implement transduce
let transduce = (transform, reducer, accumulation, collection) => {
	let transformedReducer = transform(reducer)
	for (let value of collection) {
		accumulation = transformedReducer(accumulation, value)
	}
	return accumulation
}
// console.log(transduce(cleanNumbersTransform, pushReducer, [], [1, 2, 3, 4]))

// console.log(
// 	transduce(
// 		compose(map(toUpper), filter(isVowel)),
// 		(str, char) => str + char,
// 		"",
// 		"hello world"
// 	)
// )

// into figures out how to reduce by looking at the type of the target
let into = (to, transform, collection) => {
	if (Array.isArray(to)) {
		return transduce(transform, pushReducer, to, collection)
	} else if (typeof to === "string") {
		return transduce(transform, (str, char) => str + char, to, collection)
	}
}
console.log(into([], cleanNumbersTransform, [1, 2, 3, 4]))
console.log(into("", compose(map(toUpper), filter(isVowel)), "hello world"))

// seq is similar to into, except the target type will be inferred from the source collection
let seq = (transform, collection) => {
	if (Array.isArray(collection)) {
		return transduce(transform, pushReducer, [], collection)
	} else if (typeof collection === "string") {
		return transduce(transform, (str, char) => str + char, "", collection)
	}
}
console.log(seq(cleanNumbersTransform, [1, 2, 3, 4]))
console.log(seq(compose(map(toUpper), filter(isVowel)), "hello world"))

// the beautiful thing is that we can always transduce into anything, even an
// integer
console.log(transduce(map(double), (a, b) => a + b, 0, [1, 2, 3, 4]))
