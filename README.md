# Data Transformation

* A transducer can take anything that follows the [Iterable protocol](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols)
* It's result can be anything

## Motivational example (WIP)

Something like:

```js
let validPeople = []
for(let person in people) {
  // person is in a valid age range
  if(person.age >= 18 && person.age < 28) {
    validPeople.push(person)
  }
}

for (let person in validPeople) {
  ...
}
```

Find an example with three or four steps (map, reduce and filter)

The example implementation should be close to the task description. But due to
it iterating multiple times, building up a new array every time, it is
inefficient. Show a version with one loop for everything. This is now further
away from the description. Also show how things like validAge do not have a
name.

## Tasks

* Compare with [transducers.js](https://github.com/jlongster/transducers.js)
* Compare with [Clojure API](https://clojure.github.io/clojure/clojure.core-api.html#clojure.core/map)
* Port from the standard JS Array API:
  * every (reducer)
  * find (reducer)
  * findIndex (reducer)
  * join (reducer)
  * some (reducer)
